// SchoolTableViewCell.swift
//Created by Ramya on 14/03/23.

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var schoolDBN: UILabel!
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    var school : SchoolData? {
        didSet {
            schoolDBN.text = school?.dbn
            schoolNameLabel.text = school?.schoolName
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
